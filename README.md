# Woodswatch

Woodswatch follows the story of a cutesy, custom inhabitant of the village of Wolfden and a member of the Woodswatch, an organization pledged to protect the villagers from the frightening monsters of the Wolfwoods. One day, an ancient deity known as Nalma rises from the grave and wreaks havoc across the woods. It's up to you, the only remaining member of the Woodswatch, to travel across the woods, save the surrounding villages, and restore peace to the Wolfwoods!

Woodswatch is an action-adventure role-playing game. Its core loop involves you traveling across the Wolfwoods, fighting monsters, collecting items and equipment, rescuing surrounding villages, and eventually slaying Nalma.